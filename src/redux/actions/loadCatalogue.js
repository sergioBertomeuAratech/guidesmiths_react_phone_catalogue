import axios from "axios";
export const _LOADING = "_LOADING";
export const _LOADCATALOGUE = "_LOADCATALOGUE";
export const _ERROR = "_ERROR";

export function loadCatalogue() {
  return dispatch => {
    dispatch({
      type: _LOADCATALOGUE,
      payload: { loading: true }
    });
    setTimeout(() => {
      axios
        .get("http://localhost:3000/phones")
        .then(response => {
          dispatch({
            type: _LOADCATALOGUE,
            payload: { loading: false, response: response.data }
          });
        })
        .catch(error => {
          console.log(error);
          dispatch({
            type: _ERROR,
            payload: {
              loading: false,
              response: { state: true, message: error.message }
            }
          });
        });
    }, 500);
  };
}
