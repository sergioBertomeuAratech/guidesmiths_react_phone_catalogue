import axios from "axios";
// export const type = 'loadItem';
export const _LOADING = "_LOADING";
export const _LOADITEM = "_LOADITEM";
export const _ERROR = "_ERROR";

export function loadItem(id) {
    return dispatch => {
        dispatch({
            type: _LOADITEM,
            payload: { loading: true }
        });
        setTimeout(() => {
            axios
                .get("http://localhost:3000/phones/" + id)
                .then(response => {
                    if (response.data.message) {
                        dispatch({
                            type: _ERROR,
                            payload: {
                                loading: false,
                                response: { state: true, message: response.data.message }
                            }
                        });
                    }
                    dispatch({
                        type: _LOADITEM,
                        payload: { loading: false, response: response.data }
                    });
                })
                .catch(error => {
                    console.log(error);
                    dispatch({
                        type: _ERROR,
                        payload: {
                            loading: false,
                            response: { state: true, message: error.message }
                        }
                    });
                });
        }, 500);
    };
}
