import catalogue from './reducers/catalogue';
import currentItem from './reducers/currentItem';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

const reducer = combineReducers({
    catalogue,
    currentItem,
});

const store = createStore(reducer, applyMiddleware(thunk));

export default store;