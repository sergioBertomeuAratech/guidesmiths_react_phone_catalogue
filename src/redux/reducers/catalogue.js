import { _LOADING } from '../actions/loadCatalogue';
import { _LOADCATALOGUE } from '../actions/loadCatalogue';
import { _ERROR } from '../actions/loadCatalogue';

const initialState = {
    loading: null,
    catalogue: [],
    error: {state: false, message: null}
};

function reducer(state = initialState, { type, payload }) {
    switch (type) {
        case _LOADING:
            return Object.assign({}, state, { loading: payload.loading });
        case _LOADCATALOGUE:
            return Object.assign({}, state, { loading: payload.loading, catalogue: payload.response });
        case _ERROR:
            return Object.assign({}, state, { loading: payload.loading, error: payload.response });
        default:
            return state;
    }
}

export default reducer;