import { _LOADING } from '../actions/loadItem';
import { _LOADITEM } from '../actions/loadItem';
import { _ERROR } from '../actions/loadItem';

const initialState = {
    loading: null,
    currentItem: {},
    error: { state: false, message: null }
};

function reducer(state = initialState, { type, payload }) {
    switch (type) {
        case _LOADING:
            return Object.assign({}, state, { loading: payload.loading });
        case _LOADITEM:
            return Object.assign({}, state, { loading: payload.loading, currentItem: payload.response });
        case _ERROR:
            return Object.assign({}, state, { loading: payload.loading, error: payload.response });

        default:
            return state;
    }
}

export default reducer;