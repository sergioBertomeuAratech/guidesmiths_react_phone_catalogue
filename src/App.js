import React from "react";
import { BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./redux/store";
import Catalogue from "./components/catalogue";
import Details from "./components/details";

function App() {

  return (
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route path="/catalogue" component={Catalogue} />
          <Route path="/details/:itemId" component={Details} />
          <Redirect from="/" to="/catalogue" />
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
