import React, { Component } from "react";
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { loadCatalogue } from '../../redux/actions/loadCatalogue'
import Page from "./page";


class Catalogue extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      catalogue: [],
      error: null
    };
    this.goTo = this.goTo.bind(this);
  }

  componentDidMount() {
    this.props.loadCatalogue();
  }

  goTo(path) {
    this.props.history.push(path);
  }

  render() {
    const { loading, catalogue, error } = this.props;
    return (
      <Page
        loading={loading}
        error={error}
        catalogue={catalogue}
        goTo={this.goTo}
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.catalogue.loading,
    catalogue: state.catalogue.catalogue,
    error: state.catalogue.error
  }
};

const mapDispathToProps = {
  loadCatalogue,
}

export default withRouter(
  connect(mapStateToProps, mapDispathToProps)(Catalogue)
);