import React, { Fragment } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "../appBar";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";

import "./style.css";

function Page(props) {
  const { loading, catalogue, error, goTo } = props;
  const [open, setOpen] = React.useState(error ? true : false);

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  return (
    <Fragment>
      <CssBaseline />
      <AppBar />

      <div className="results-page">
        {loading ? (
          <Box mt={5}>
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
              spacing={9}
            >
              <Grid item>
                <CircularProgress size={100} />
              </Grid>
              <Grid item>
                <Typography variant="h5" component="h2">
                  Loading...
                </Typography>
              </Grid>
            </Grid>
          </Box>
        ) : error.state ? (
          <Snackbar
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "center"
            }}
            open={open}
            autoHideDuration={6000}
            onClose={handleClose}
          >
            <Alert onClose={handleClose} severity="error">
              {error.message}
            </Alert>
          </Snackbar>
        ) : (
          catalogue.map(item => (
            <div key={item.id} className="card-container">
              <Card
                className="card"
                onClick={() => goTo(`/details/${item.id}`)}
              >
                <CardActionArea>
                  <CardMedia
                    title={item.name}
                    image={"http://localhost:3000/images/" + item.imageFileName}
                    className="card-media"
                  />
                </CardActionArea>
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    {item.name}
                  </Typography>
                  <Typography component="p">{item.description}</Typography>
                </CardContent>
              </Card>
            </div>
          ))
        )}
      </div>
    </Fragment>
  );
}

export default Page;
