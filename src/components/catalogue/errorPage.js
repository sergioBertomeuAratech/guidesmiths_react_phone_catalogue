import React, { Fragment } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import { spacing } from '@material-ui/system';
import AppBar from "../appBar";
import Box from '@material-ui/core/Box';
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';


function ErrorPage(props) {
    const { error } = props;
    // const [open, setOpen] = React.useState(false);
    // const handleClick = () => {
    //     setOpen(true);
    // };
    // if (error) {
    //     handleClick();
    // }

    const [open, setOpen] = React.useState(error ? true : false);
    function Alert(props) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
    }
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };

    return (
        <Fragment>
            <CssBaseline />
            <AppBar />
            <Box mt={5}>
                <Grid
                    container
                    direction="column"
                    justify="center"
                    alignItems="center"
                    spacing={9}
                >
                    <Grid item>
                        <CircularProgress size={100} />
                    </Grid>
                    <Grid item>
                        <Typography variant="h5" component="h2">
                            Loading...
                    </Typography>
                    </Grid>
                </Grid>
            </Box>
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                open={open}
                autoHideDuration={6000}
                onClose={handleClose}
            >
                <Alert onClose={handleClose} severity="error">
                    {error}
                </Alert>
            </Snackbar>
        </Fragment>
    )

}

export default ErrorPage;