
import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Typography from '@material-ui/core/Typography';
import AccountCircle from '@material-ui/icons/AccountCircle';
import "./style.css";

function Page(props) {

    return (
        <AppBar position="static">
            <Toolbar className="appbar">
                <IconButton edge="start" color="inherit" aria-label="menu">
                    <MenuIcon />
                </IconButton>
                <Typography variant="h6" color="inherit">
                    Phone Catalogue
                </Typography>
                <AccountCircle />
            </Toolbar>
        </AppBar>
    );
}

export default Page;