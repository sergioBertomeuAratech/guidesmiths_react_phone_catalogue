import React, { Fragment } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "../appBar";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";
import List from "@material-ui/core/List";
import { ListSubheader } from "@material-ui/core";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import "./style.css";

function Page(props) {
  const { loading, currentItem, error, goTo } = props;
  const [open, setOpen] = React.useState(error ? true : false);

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  return (
    <Fragment>
      <CssBaseline />
      <AppBar />

      <div className="details-page">
        {loading ? (
          <Box mt={5}>
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
              spacing={9}
            >
              <Grid item>
                <CircularProgress size={100} />
              </Grid>
              <Grid item>
                <Typography variant="h5" component="h2">
                  Loading...
                </Typography>
              </Grid>
            </Grid>
          </Box>
        ) : error.state ? (
          <div>
            <Snackbar
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "center"
              }}
              open={open}
              autoHideDuration={6000}
              onClose={handleClose}
            >
              <Alert onClose={handleClose} severity="error">
                {error.message}
              </Alert>
            </Snackbar>
          </div>
        ) : (
          <Paper elevation={1} className="paper-container">
            <Fragment>
              <Grid
                container
                direction="row"
                justify="flex-end"
                alignItems="center"
              >
                <IconButton
                  aria-label="close"
                  onClick={() => goTo("/catalogue")}
                >
                  <CloseIcon fontSize="inherit" />
                </IconButton>
              </Grid>

              <Typography variant="overline" className="content">
                {currentItem.manufacturer}
              </Typography>
              <Typography gutterBottom variant="h5" component="h2">
                {currentItem.name}
              </Typography>

              <div
                className="item-image"
                style={{
                  backgroundImage: `url(${"http://localhost:3000/images/" +
                    currentItem.imageFileName})`
                }}
              />
              <Typography variant="caption" className="content">
                Description
              </Typography>
              <Typography gutterBottom component="p" className="content">
                {currentItem.description}
              </Typography>

              <List>
                <ListSubheader>
                  <Typography variant="caption">Processor</Typography>
                </ListSubheader>
                <ListItem>
                  <ListItemText primary={currentItem.processor}></ListItemText>
                </ListItem>
                <Divider />
                <ListSubheader>
                  <Typography variant="caption">RAM</Typography>
                </ListSubheader>
                <ListItem>
                  <ListItemText
                    primary={currentItem.ram + " GB"}
                  ></ListItemText>
                </ListItem>
                <Divider />
                <ListSubheader>
                  <Typography variant="caption">Screen</Typography>
                </ListSubheader>
                <ListItem>
                  <ListItemText primary={currentItem.screen}></ListItemText>
                </ListItem>
                <Divider />
                <ListSubheader>
                  <Typography variant="caption">Price</Typography>
                </ListSubheader>
                <ListItem>
                  <ListItemText
                    primary={currentItem.price + " $"}
                  ></ListItemText>
                </ListItem>
                <Divider />
              </List>
              <Button className="button" onClick={() => goTo("/catalogue")}>
                Back
              </Button>
            </Fragment>
          </Paper>
        )}
      </div>
    </Fragment>
  );
}

export default Page;
