import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { loadItem } from "../../redux/actions/loadItem";
import Page from "./page";

class Details extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      currentItem: {},
      error: null
    };
    this.goTo = this.goTo.bind(this);
  }

  componentDidMount() {
    this.props.loadItem(this.props.match.params.itemId);
  }

  goTo(path) {
    this.props.history.push(path);
  }

  render() {
    const { loading, error, currentItem } = this.props;
    return (
      <Page
        loading={loading}
        error={error}
        currentItem={currentItem}
        goTo={this.goTo}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.currentItem.loading,
    currentItem: state.currentItem.currentItem,
    error: state.currentItem.error
  };
};

const mapDispathToProps = {
  loadItem
};

export default withRouter(connect(mapStateToProps, mapDispathToProps)(Details));
